# 案例演示

本章将演示几个基于RIL开发的案例，包括:

- TCP通信
- UDP通信
- HTTP文件下载
- 短信收发

## TCP 通信

该示例主要演示了TCP通信基本流程，包含服务器连接，数据收发，完整的代码可以参考/demo/user/samples/sample_tcp.c。

建立一个TCP通信程序需要完成以下几个步骤：

1. 定义socket
2. 创建socket，并指定事件处理程序及接收缓冲区大小
3. 等待模组注册上线
4. 启动服务器连接（指定主机、端口、连接类型）

```c
//...
/**
 * @brief       tcp 任务
 * @return      none
 */ 
static void tcp_task(void)
{
    int  retry = 0;
    bool result;
    ril_socket_t sockfd;                          //socket
    sockfd = ril_sock_create(socket_event, 256);  //创建socket，并设置256 Bytes的接收缓冲区
    printf("TCP socket create %s.\r\n", sockfd != RIL_INVALID_SOCKET ? "OK" : "ERR");
    if (sockfd == 0) {
        return;
    }
    while (1) {
        os_sleep(10);
        if (!ril_isonline())                       //等待网络连接
            continue;
        if (!ril_sock_online(sockfd)) {             
            result = ril_sock_connect(sockfd, TCP_SERVER, TCP_PORT, RIL_SOCK_TCP);
            if (result != RIL_OK) {
                retry %= 10;
                os_sleep(10000 * retry++);        //连续失败等待一段时间再尝试
            } 
            printf("TCP Socket connect %s.\r\n", result == RIL_OK ? "OK" : "ERR");
        } else {
           recv_data_process(sockfd);             //数据接收处理
           send_data_regularly(sockfd);           //数据发送处理
        }
    }
}
task_define("tcp-sample", tcp_task, 256, 6);     //定义TCP任务
```



## UDP 通信

该示例主要演示了UDP通信基本流程，包含服务器连接，数据收发功能，完整的代码可以参考/demo/user/samples/sample_udp.c。

建立一个UDP通信程序需要完成以下几个步骤：

1. 定义socket
2. 创建socket，并指定事件处理程序及接收缓冲区大小
3. 等待模组注册上线
4. 启动服务器连接（指定主机、端口、连接类型）

```c
//......
/** 
 * @brief       udp 任务
 * @return      none
 */ 
static void udp_task(void)
{
    int  retry = 0;
    bool result;
    ril_socket_t sockfd;                          //socket
    sockfd = ril_sock_create(socket_event, 256);  //创建socket，并设置256 Bytes的接收缓冲区
    printf("UDP socket create %s.\r\n", sockfd != RIL_INVALID_SOCKET ? "OK" : "ERR");
    if (sockfd == 0) {
        return;
    }
    while (1) {
        os_sleep(10);
        if (!ril_isonline())                       //等待网络连接
            continue;
        if (!ril_sock_online(sockfd)) {             
            result = ril_sock_connect(sockfd, UDP_SERVER, UDP_PORT, RIL_SOCK_UDP);
            if (result != RIL_OK) {
                retry %= 10;
                os_sleep(10000 * retry++);        //连续失败等待一段时间再尝试
            } 
            printf("UDP Socket connect %s.\r\n", result == RIL_OK ? "OK" : "ERR");
        } else {
           recv_data_process(sockfd);             //数据接收处理
           send_data_regularly(sockfd);           //数据发送处理
        }
    }
}
task_define("udp-sample", udp_task, 256, 6);     //定义UDP任务
```

## 短信收发

!> 注:在使用短信功能前需要先确保您的SIM卡已经开通短信业务。

下面示例主要演示了短信发送/接收功能，完整的例子可以参考/demo/user/samples/sample_sms.c。

### 发送短信

对于短信发送功能，使用<ril_sms_send>实现，它的接口定义如下：

```c
/**
 * @brief     发送短信
 * @param[in] phone  - 目标手机号
 * @param[in] msg    - 短信消息( < 164 bytes)
 * @return    RIL_OK - 发送成功, 其它值 - 异常
 */
int ril_sms_send(const char *phone, const char *msg);
```

**使用范例：**

下面是一个使用命令行发送短信的例子，用户只需要在串口终端中输入相应的命令即可完成短信发送功能。

命令格式如下：

sms+目标手机号+短信内容

```c
//...
/*
 * @brief   sms短信发送命令
 *          命令格式:sms,phone,message text
 * @example sms,18512344321,sms test
 */    
static int do_cmd_sms(struct cli_obj *cli, int argc, char *argv[])
{
    bool res;
    if (!ril_isreg()) {
        cli->print(cli, "unreg to network.");      //未注册到网络,无法发送短信
        return -1;
    }    
    if (argc != 3) {
        cli->print(cli, "Command format error!!!\r\n"
                 "format:sms,phone,message text.\r\n"
                 "Example:sms,18512344321,sms test.\r\n");
        return -1;
    }
    res = ril_sms_send(argv[1], argv[2]);
    cli->print(cli, "sms send %s\r\n", res ? "OK" : "ERROR");
    return 0;
} 
cmd_register("sms", do_cmd_sms, "send sms");     //注册短信发送命令

```

### 接收短信

对于短信接收功能，则是通过订阅RIL通知来实现。

```c
/*
 * @brief   短信接收处理
 */
static void sms_recv_handler(sms_info_t *sms, int data_size)
{
    printf("Receive sms=> \r\nphone:%s\r\nText:%s\r\n",sms->phone, sms->msg);
}
ril_on_notify(RIL_NOTIF_SMS, sms_recv_handler);
```



## HTTP文件下载

HTTP是RIL内置的一个组件，应用于远程文件下载及FOTA，它的基本特性如下：

- 支持下载超时控制，在规定时间段时如果网络断开会自动重连。
- 支持断点续传功能，即使由于网络原因中断，下一次还可以接着下载。

完成一个HTTP文件功能功能需要执行以下几个步骤：
1. 通过http_client_create创建http_client，并指定事件回调接口、下载地址及超时时间。
2. 通过http_start_download启动下载。
4. 下载结束后，通过http_client_destroy销毁http_client。

### HTTP 事件

启动文件下载请求之后，HTTP组件将以事件回调的形式将下载相关状态、数据递交到上层应用程序。事件的原型定义如下：

```c
typedef void (*http_event_t)(http_event_args_t *args); /* http 事件回调*/
```
#### HTTP事件参数

```c
/*HTTP 事件参数 ---------------------------------------------------------------*/
typedef struct {
    struct http_client *client;                             
    unsigned char state;                                /* 当前状态   */
    unsigned int  filesize;                             /* 文件大小   */
    unsigned int  spand_time;                           /* 已使用时间 */    
    unsigned int  offset;                               /* 写指针偏移 */
    unsigned char *data;                                /* 数据指针   */
    unsigned int  datalen;                              /* 数据长度   */
}http_event_args_t;
```

#### HTTP状态

```c
/* http 状态 -----------------------------------------------------------------*/
#define HTTP_STAT_START          0                      /* 开始下载 */
#define HTTP_STAT_DATA           1                      /* 接收数据*/
#define HTTP_STAT_DONE           2                      /* 下载完成*/
#define HTTP_STAT_FAILED         3                      /* 下载失败*/
```

### 示例代码

?> 在使用这个功能前，需要先搭建一个HTTP文件服务器，笔者使用的是[Http File Server]，大家自行到网上下载。

下面示例演示了通过命令行控制文件下载的功能，用户可以在终端串口上执行命令[http,url,120]执行下载，如果执行成功会在串口终端上打印下载的进度信息，完整的代码可以参考/demo/user/samples/sample_http.c。

```c
//.....
/**
 * @brief       http 事件处理
 */
static void http_event(http_event_args_t *e)
{
    unsigned int recvsize = e->offset + e->datalen;
    if (e->state == HTTP_STAT_DATA) {
        printf("%d/%d bytes %.1f%% completed.\r\n", recvsize , e->filesize, 
                 100.0 * recvsize/ e->filesize);
        
        //write(file, e->data, e->datalen);
        
    }    
    if (e->state == HTTP_STAT_DONE)
        printf("\r\nDownload complete, spand time:%d\r\n", e->spand_time);
}

/**
 * @brief   http文件下载命令
 *          命令格式:http,host,port,filename, timeout(s)
 * @example http,123.146.152.12,1234,/ril-demo.hex,120
 */    
static int do_cmd_http(struct cli_obj *cli, int argc, char *argv[])
{
    http_client_t *http;
    const char *host, *file;
    int port, timeout;
    if (argc < 5) {
        cli->print(cli, "Command format error!!!\r\n"
                 "Format:http,host,port,filename, timeout(s)\r\n"
                 "Example:http,123.146.152.12,1234,/ril-demo.hex,120\r\n");
        return -1;
    }
    host = argv[1];
    port = atoi(argv[2]);
    file = argv[3];
    timeout = atoi(argv[4]);
    
    cli->print(cli, "Download file [%s] from [%s].\r\n", file, host);
    
    //创建HTTP客户端
    http = http_client_create(http_event, host, port);
    if (http == NULL) {
        cli->print(cli, "Input error, http client create failed.\r\n");
        return -1;
    }
    http_start_download(http, file, timeout);             //启动HTTP下载
    http_client_destroy(http);                            //销毁客户端
    return 0;
                      
}cmd_register("http", do_cmd_http, "http file download"); //注册http命令
```

## MQTT

~~等待补充~~